#!/usr/bin/env bash

# The init script used to download the appropriate versions of rusty things

source /etc/environment

cd /root

mkdir rusty-clock rusty-pipe

cd rusty-clock

wget https://galois.network/mirror/community/rusty-clock

chmod +x ./rusty-clock

tmux new-session -d -s "rusty-clock" /root/rusty-clock/rusty-clock

cd /root/rusty-pipe

# Download rusty-pipe
wget https://galois.network/mirror/community/Minecraft/sponge-vanilla/rusty-pipe-mc-spongevanilla-6.1.0-beta27

# Download init/run scripts
wget https://galois.network/mirror/community/Minecraft/sponge-vanilla/spongevanilla-6.1.0-beta27.zip
apt-get install --yes --force-yes zip
unzip spongevanilla-6.1.0-beta27.zip
rm spongevanilla-6.1.0-beta27.zip

chmod +x ./rusty-pipe-mc-spongevanilla-6.1.0-beta27

tmux new-session -d -s "rusty-pipe" /root/rusty-pipe/rusty-pipe-mc-spongevanilla-6.1.0-beta27

# Create systemd files

read -r -d '' RCSYSTEMD << EOM
[Unit]
Description=Rusty Clock service

[Service]
ExecStart=/root/rusty-clock/rusty-clock

[Install]
WantedBy=multi-user.target
EOM

echo "$RCSYSTEMD" > /etc/systemd/system/rusty-clock.service

read -r -d '' RPSYSTEMD << EOM
[Unit]
Description=Rusty Pipe service

[Service]
ExecStart=/root/rusty-pipe/rusty-pipe-mc-spongevanilla-6.1.0-beta27

[Install]
WantedBy=multi-user.target
EOM

echo "$RPSYSTEMD" > /etc/systemd/system/rusty-pipe.service

systemctl daemon-reload

systemctl enable rusty-clock rusty-pipe
