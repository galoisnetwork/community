#!/usr/bin/env bash

# Creates a non-root steam user account and uses that
# to download the steamCMD client in /home/steam/steamcmd/steamcmd.sh

RUSTY_PIPE_DIR=$(pwd)

apt-get --yes --force-yes install zip

apt-get update && apt-get install lib32gcc1 libvorbisfile3

# Get hashed password of the pass "steampass"
STEAMPASS=$(echo steampass | openssl passwd -stdin -crypt)

# TODO: Remove sudo auth once local testing is done:
# create steam user with the pass "steampass"
echo euwbah28 | sudo -S useradd -m -p ${STEAMPASS} steam

read -r -d '' CMDS << EOM
# Running the following commands as steam account:
# PWD: /home/steam
mkdir steamcmd
cd steamcmd
wget https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz
tar -zxvf steamcmd_linux.tar.gz
EOM

# TODO: Remove sudo auth once local testing is done:
# Run the above commands as the 'steam' user in /home/steam PWD
# This will yield /home/steam/steamcmd/steamcmd.sh
echo euwbah28 | sudo -S su - steam -c "$CMDS"