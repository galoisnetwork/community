#!/usr/bin/env bash

# This script is used for both installation and updating of the starbound server

# Assumes steamcmd is installed at /home/steam/steamcmd/steamcmd.sh
# Before running this script, ensure that login credentials are acquired!
# Expects $USERNAME, $PASSWORD, and $GUARDCODE

# stdout signals to get process status
# Outputs <SERVER INSTALL> before executing steam login and server install
# Outputs <SERVER INSTALL SUCCESS> after successfully logging in and installing the server

# stdout signals from steamcmd.sh:
# "Logged in OK" upon successful authentication,

# "Login Failure: Invalid Password"
#       --> invalid username or password
# "Steam Guard code:"
#       --> request for guard code (can input via stdin)
# "Login Failure: Invalid Login Auth Code"
#       --> incorrect guard code
# "Login Failure: Expired Login Auth Code"
#       --> entered expired guard code
# "ERROR! Failed to install app '211820' (No subscription)"
#       --> the user did not buy the game

# Run the following commands as steam account (pwd will init at /home/steam)
# This will create the starbound server executable at /home/steam/linux/starbound_server

RUSTY_PIPE_DIR=$(pwd)

dpkg --add-architecture i386
apt-get update
apt-get install libc6:i386 libncurses5:i386 libstdc++6:i386 --yes --force-yes


read -r -d '' CMDS << EOM
echo '<SERVER INSTALL>'
echo $(whoami)
echo "$USERNAME"
cd /home/steam/steamcmd
./steamcmd.sh +login "${USERNAME}" "${PASSWORD}" "${GUARDCODE}" +force_install_dir '/home/steam/' +app_update 211820 +quit
echo $? > exitcode.txt
echo '<SIGNING OUT STEAM>'
EOM

# TODO: Remove after testing
echo euwbah28 | sudo -S echo hi
sudo su steam -c "$CMDS"

#su - steam -c "$CMDS"

EXIT_CODE=`cat /home/steam/steamcmd/exitcode.txt`

echo "Installation exited with code: ${EXIT_CODE}"

# $? will yield the result code of the last operated command, which in this case,
# the ./steamcmd.sh command
if ((EXIT_CODE == 0))
then
    # If all goes well, ./run.sh will now be executed

    cd $RUSTY_PIPE_DIR

    $RUN_SCRIPT_PATH

fi

# exit if operation failed - rusty-pipe will be able to find out what went wrong
# using game stdout and respond accordingly