#!/usr/bin/env bash

# This is used to actually start the game server.
# Expects /home/steam/linux/starbound_server to exist
# Runs starbound_server as the non-root steam user.

read -r -d '' CMDS << EOM
cd /home/steam/linux
echo "<STARTING SERVER>"
./starbound_server
EOM

echo euwbah28 | sudo -S su - steam -c "${CMDS}"

